export const LOGIN = 'LOGIN'
export const REFRESH = 'REFRESH'
export const LOGOUT = 'LOGOUT'
export const SET_PROFILE = 'SET_PROFILE'
export const SET_CURRENT_TABLE = 'SET_CURRENT_TABLE'

export const ADD_EMAIL = 'ADD_EMAIL'
export const ADD_PASSWORD = 'ADD_PASSWORD'
