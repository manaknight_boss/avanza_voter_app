import * as ActionTypes from './constants'


export function loginAction(bearerToken, tokenExpiration, refreshToken ) {
    return {
        type: ActionTypes.LOGIN,
        payload: {
	  bearerToken,
	  tokenExpiration,
	  refreshToken
	}
    }
}

export function refreshAction(bearerToken, tokenExpiration ) {
    return {
        type: ActionTypes.REFRESH,
        payload: {
	  bearerToken,
	  tokenExpiration,
	}
    }
}

export function setProfile(payload) {
    return {
        type: ActionTypes.SET_PROFILE,
        payload
    }
}

export function setCurrentTable(payload) {
  return {
    type: ActionTypes.SET_CURRENT_TABLE,
    payload
  }
}
