export const logger = () => next => action => {
  console.log('DISPATCH ->', action.type, action.payload, '\n')
  const value = next(action)
  return value
}
