import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import mainReducer from './mainReducer'
import {logger} from './middleware'

const store = createStore(
  mainReducer, 
  {
    bearerToken: "",
    tokenExpiration: "",
    refreshToken: "",
    profile: null,
    currentTable: null,
    email: "",
    password: "",
  },
  applyMiddleware( logger )
)
export default store

