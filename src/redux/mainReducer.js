import { 
  LOGIN,
  LOGOUT,
  REFRESH,
  SET_PROFILE,
  SET_CURRENT_TABLE,
} from './constants'


export default function ( state, action )
{
  const {type, payload} = action
  switch (type) {
    case LOGIN: return {
	...state,
	...payload
      }

    case REFRESH: return {
      ...state,
      ...payload
    }

    case LOGOUT: return {
	...state,
	bearerToken: "",
	tokenExpiration: "",
	refreshToken: ""
      }

    case SET_PROFILE: return {
	...state,
	profile: payload
      }

    case SET_CURRENT_TABLE: return {
	...state,
	currentTable: payload
      }

    default: return state;
  } 
}
