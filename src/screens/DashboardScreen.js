import { Button, Text } from 'react-native-paper';
import { StyleSheet, FlatList, View, ScrollView, RefreshControl, Alert } from 'react-native';
import {NavigationEvents} from 'react-navigation'

import React from 'react';
import styled from 'styled-components/native'

import { Row, Col, Table, TableHead } from '../ui/tabular';
import { theme } from '../theme';
import Header from '../ui/Header';
import {getTableResults} from '../tools/api'
import {connect} from 'react-redux'
import Loading from '../ui/Loading'
import {getCurrentTable} from '../tools/api'

function DashboardScreen(props) {
  const {profile, navigation} = props
  const [coordinators, setCoordinators] = React.useState([])
  const [voters, setVoters] = React.useState(0)
  const [voted, setVoted] = React.useState(0)
  const [notVoted, setNotVoted] = React.useState(0)
  const [loading, setLoading] = React.useState(false);
  const [refreshing, setRefreshing] = React.useState(false)

  const getCoordinators = async () => {
      const results = await getTableResults(getCurrentTable())
      setCoordinators(results.cordinators)
      setVoters(results.total_voters)
      setVoted(results.total_voted)
      setNotVoted(results.total_not_voted)
  }

  const renderItem = ({item, index, separators}) => {
    return (
      <Row odd={index%2===0}>
      <Col><Text>{item.first_name} {item.last_name}</Text></Col>
      <Col centered><Text>{item.left}</Text></Col>
      <Col centered>
      <Button 
      icon="magnify"
      onPress={() => navigation.push("CoordinatorScreen", {coordinator:item})}
      >
      Ver
      </Button> 
      </Col>
      </Row>
    )
  }

  const loadData = async () => {
    try {
      setLoading(true)
      await getCoordinators()
    } catch(error) {
      Alert.alert(null, error.message)
    }
    finally {
      setLoading(false)
    }
  }

  const refresh = async () => {
    try {
      setRefreshing(true)
      await getCoordinators()
    } catch(error) {
      Alert.alert(null, error.message)
    }
    finally {
      setRefreshing(false)
    }
  }

  if (loading) {
    return <Loading />
  }

  return (
      <>
	<NavigationEvents onWillFocus={payload => loadData()} />       
	<Header title="Resumen" navigation={navigation} />

            <Stats>
                <OverviewRow>
                    <Label>Votantes</Label>
                    <Value>{voters}</Value>
                </OverviewRow>
                <OverviewRow>
                    <Label>Por Votar</Label>
                    <Value>{notVoted}</Value>
                </OverviewRow>
                <OverviewRow>
                    <Label>Votaron</Label>
                    <Value>{voted}</Value>
                </OverviewRow>
            </Stats>

            <TableHeader>
                <TableHeaderText>
                    POR VOTAR (POR COORDINADOR)
                </TableHeaderText>
            </TableHeader>

            <TableHead headings={['Coordinador', 'Votantes', 'Acci\u00f3n']} />
	    <FlatList
	      data={coordinators}
	      keyExtractor={item => item.id}
	      renderItem={renderItem} 
	      removeClippedSubviews={true}
	      initialNumToRender={7}
	      maxToRenderPerBatch={7}
	      updateCellsBatchingPeriod={1000}
	      refreshing={refreshing}
	      onRefresh={refresh}
	    />
      		
        </>
    )
}

export default connect(
    ({profile}) => ({profile})
)(DashboardScreen)

export const Label = styled(({children, ...props}) => {
    return (
        <View {...props}>
            <LabelText>{children}</LabelText>
        </View>
    )
})` `

export const Value = styled(({children, ...props}) => {
    return (
        <View {...props}>
            <ValueText>{children}</ValueText>
        </View>
    )
})` `

export const Stats = styled.View`
padding: 15px 0;
flex-direction: row;
justify-content: space-evenly;
`

export const OverviewRow = styled.View`
align-items: center;
`

export const LabelText = styled(Text)`
font-size: 16px;
`

export const ValueText = styled(Text)`
font-size: 25px;
color: ${theme.colors.primary};
`

export const TableHeader = styled.View`
align-items: center;
background-color: #F3F3F3;
padding: 13px 0;
`

export const TableHeaderText = styled(Text)`
font-size: 16px;
`


const S = StyleSheet.create({
  firstCol: {

  }
})


/*
<View style={{flex:1}}>
    <ScrollView>
	{coordinators.map((coordinator, r) => {
	    const length = coordinators.length
	    const isLast = i => i % length  === length -1 && !Action
	    return (
		<Row odd={r%2 === 0} key={coordinator.id}>
		    <Col style={{justifyContent:'center'}}><Text>{coordinator.first_name} {coordinator.last_name}</Text></Col>
		    <Col centered><Text>{coordinator.left}</Text></Col>
		    <Col centered>
			<Button 
			    icon="magnify"
			    onPress={() => navigation.push("CoordinatorScreen", {coordinator})}
			>
			    Ver
			</Button> 
		    </Col>
		</Row>
	    )
	})}
    </ScrollView>
</View>
 */
