import axios from 'axios'
import { Headline, Subheading, Button } from 'react-native-paper';
import { Alert, StyleSheet } from 'react-native';
import React from 'react';
import styled from 'styled-components/native'

import Content from '../../ui/Content';
import FormInput from '../../ui/FormInput';
import PrimaryButton from '../../ui/PrimaryButton';
import { connect } from 'react-redux'
import {  bindActionCreators} from 'redux'

import { login, getProfile, refresh } from '../../tools/api'
import { setBearerToken, setProfile } from '../../redux/actions'
import store from '../../redux/store'
import { client }  from '../../tools/api'
import Loading from '../../ui/Loading'

function LoginScreen(props) {
  const {
    email, 
    password, 
    navigation,
    setProfile,
  } = props

  const [loginEmail, setLoginEmail] = React.useState("")
  const [loginPassword, setLoginPassword] = React.useState("")
  const [loading, setLoading] = React.useState(false)

  const submit = async () => {
    let profile = null
    try {
      setLoading(true)
      await login(loginEmail, loginPassword)
      profile = await getProfile()
    }
    catch(error) {
      Alert.alert("Error", error.message)
    }
    finally  {
      setLoading(false)
    }

    if (profile) {
      setProfile(profile)
      const {tokenExpiration} = store.getState()
      const time = 60000
      setTimeout(refresh, time)
      navigation.navigate('App')
    }
  }


  return (
    <>
    <Content contentContainerStyle={S.content}>
      <Headline>Bienvenido(a)</Headline>
      <Subheading style={S.gray}>Accede para continuar</Subheading>

      <Form>
        <FormInput
    value={loginEmail}
    onChangeText={setLoginEmail}
          label="Cédula"
    	  disabled={loading}
        />
        <FormInput
          label="Contrase&ntilde;a"
	  value={loginPassword}
	  onChangeText={setLoginPassword}
	  secureTextEntry
    	  disabled={loading}
        />
      </Form>
      <PrimaryButton 
        labelStyle={{color: 'white'}}
        mode="contained"
        onPress={submit} 
    	disabled={loading}
      >
        Acceder
      </PrimaryButton>
      <Button style={{marginTop: 20}}
                        onPress={() => navigation.navigate('Register')}>
                        Registrarse
                    </Button>
    </Content>
    </>
  )
}

export default connect(
    ({ tokenExpiration}) => ({ tokenExpiration}),
    dispatch => bindActionCreators({
      setProfile,
    }, dispatch)
)(LoginScreen)

const Form = styled.View`
margin: 30px 0;
`

const S = StyleSheet.create(
  {
    gray: {
      color: '#C4C4C4',
    },
    content: {
      flex: 1,
      justifyContent: 'center', 
    }
  }
)
