import axios from 'axios'
import { Headline, Subheading, Button } from 'react-native-paper';
import { Alert, StyleSheet } from 'react-native';
import React from 'react';
import styled from 'styled-components/native'

import Content from '../../ui/Content';
import FormInput from '../../ui/FormInput';
import PrimaryButton from '../../ui/PrimaryButton';
import { connect } from 'react-redux'
import {  bindActionCreators} from 'redux'

import { login, getProfile, refresh } from '../../tools/api'
import { setBearerToken, setProfile } from '../../redux/actions'
import store from '../../redux/store'
import { client }  from '../../tools/api'
import Loading from '../../ui/Loading'

function Register(props) {
  const {
    email, 
    password, 
    navigation,
    setProfile,
  } = props

  const [loginEmail, setLoginEmail] = React.useState("")
  const [loginPassword, setLoginPassword] = React.useState("")
  const [loading, setLoading] = React.useState(false)


  return (
    <>
    <Content contentContainerStyle={S.content}>
      <Subheading style={S.gray}>Unidos Avancemos por un mejor futuro</Subheading>

      <Form>
        <FormInput
    value={loginEmail}
    onChangeText={setLoginEmail}
          label="Cédula"
    	  disabled={loading}
        />
      </Form>
      <PrimaryButton 
        labelStyle={{color: 'white'}}
        mode="contained"
        onPress={() => Alert.alert("Información", "Para ser coordinador de avanza debes unirte a un equipo de Avanza. Procede nuestra página web y únete ya!")} 
    	disabled={loading}
      >
        Registrarse
      </PrimaryButton>
      <Button style={{marginTop: 20}}
                        onPress={() => navigation.goBack()}>
                        Espalda
                    </Button>
    </Content>
    </>
  )
}

export default connect(
    ({ tokenExpiration}) => ({ tokenExpiration}),
)(Register)

const Form = styled.View`
margin: 30px 0;
`

const S = StyleSheet.create(
  {
    gray: {
      color: '#C4C4C4',
    },
    content: {
      flex: 1,
      justifyContent: 'center', 
    }
  }
)
