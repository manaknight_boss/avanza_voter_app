import {View, Alert} from 'react-native'
import { Button, Headline, List } from 'react-native-paper';
import React from 'react';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Content from '../ui/Content';
import FormInput from '../ui/FormInput';
import Header from '../ui/Header';
import PrimaryButton from '../ui/PrimaryButton';
import {addTurns, getCurrentTable} from '../tools/api'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import Loading from '../ui/Loading'
import store from '../redux/store'

function HomeScreen(props) 
{
  console.log("props",props);
  const { navigation } = props
  const [numbers, setNumbers] = React.useState(Array(5).fill(null))
  const [loading, setLoading] = React.useState(false)
  const {profile} = store.getState()
  const {currentTable} = store.getState()
  console.log("table", profile["table"+currentTable]);

  const addNumber = () => {
    const tmpNumbers = [...numbers]
    tmpNumbers.push("")
    setNumbers(tmpNumbers)
  }
 
  
  // var currentPageSelected = getCurrentTable()

  const setNumberValue = (i, value) => {
    const tmp = [...numbers]
    tmp[i] = value
    setNumbers(tmp)
  }

  componentWillReceiveProps = (props) => {
    console.log("change");
    if(props.navigation.state.isDrawerOpen){
         console.log("open");
    }
}

componentDidUpdate = (prevProps) => {
  console.log("hello");
  // if (prevProps.text !== this.props.text) {
  //   this.updateAndNotify();
  // }
}

  const send = async () => {
    try {
      setLoading(true)
      await addTurns(numbers, getCurrentTable())
      //currentPageSelected= getCurrentTable();
      //console.log("Page",currentPageSelected);
      setNumbers(Array(5).fill(null))
      Alert.alert(null,"Turnos enviados exitosamente")
    }
    catch(error) {
      Alert.alert("Error", error.message)
    }
    finally {
      setLoading(false)
    }
  }

  return (
    <>
      <Header title={"Ingresar Turnos"} navigation={navigation}/>
      {loading ? <Loading /> : (
      <View style={{flex:1}}>
	<Content>
	  {numbers.map((n,i) => {
	    const label = `Turno ${i+1}`
	    return (
	      <FormInput
		key={i}
		label={label}
		value={n}
		onChangeText={value => setNumberValue(i, value)}
		mode="outlined"
	      />
	    )
	  })}
	</Content>

	<SideMargin>
	  <PrimaryButton secondary onPress={send} > Enviar </PrimaryButton>
	</SideMargin>
	</View>
      )}
    </>
  )
}

const HomeContainer = connect(
    ({profile}) => ({profile}),
)(HomeScreen)

HomeContainer.navigationOptions = {
  drawerLabel: 'Agregar Turnos',
  drawerIcon: (focused, tintColor) => <Icon name="textbox" size={24} color={tintColor} />,
}
export default HomeContainer;

const StyledHeadline = styled(Headline)`
font-weight: bold;
margin: 15px 0;
`

const ButtonNew = styled(Button)`
margin: 20px 0;
`

const SideMargin = styled.View`
margin: 0 25px;
`
