import { Text } from 'react-native-paper';
import { View, StyleSheet, Alert } from 'react-native';
import React from 'react';
import styled from 'styled-components/native'

import { Table, TableHead } from '../ui/tabular';
import { theme } from '../theme';
import Header from '../ui/Header';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import PrimaryButton from '../ui/PrimaryButton'
import { Camera } from 'expo-camera';
import * as Permissions from 'expo-permissions'
import {uploadFinalCount, getCurrentTable} from '../tools/api'
import { ActivityIndicator } from 'react-native-paper';

function FinalCount({navigation}) {
  const [hasPermission, setHasPermission] = React.useState(null);
  const [loading, setLoading] = React.useState(false)
  const [camera, setCamera] = React.useState(null)

  const getPermissions = async () => {
    const permission = await Permissions.askAsync(Permissions.CAMERA) 
    console.log(permission, '\n')
    if (permission.status === 'granted') return setHasPermission(true)
    throw new Error('Debes dar permiso de cámara para usar esta función')
  }

  const send = async () => {
    try {
      if (!hasPermission) throw new Error(
	'Debes otorgar permisos de cámara')
      const {uri} = await camera.takePictureAsync();
      setLoading(true)
      camera.pausePreview()
      const success = await uploadFinalCount(getCurrentTable(), uri)
      Alert.alert(null, "La imagen ha sido enviada")
    }
    catch(error) {
      Alert.alert("Error", error.message)
    }
    finally {
      camera.resumePreview()
      setLoading(false)
      setHasPermission(false)
    }
  }

  React.useEffect(() => {
    try {
      getPermissions()
    }
    catch(error) {
      Alert.alert("Error de permiso", error.message)
      console.warning(error)
    }
    finally {
      setLoading(false)
    }
  }, [hasPermission]);

  return (
    <>
      <Header title="Cuenta Final" navigation={navigation} />
      <View style={{flex:1,}} >
    	{hasPermission &&
	   <Camera 
	      ref={ref => setCamera(ref)}
	      type={Camera.Constants.Type.back}
	      style={{
		flex: 1, 
		alignItems:'center',
		justifyContent:'center',
	      }} 
	    >

	  {loading && 
	    <ActivityIndicator 
	      size="large" 
	      animating={true} 
	      color="orange" 
	    /> 
	  }
	   </Camera> 	
	}
      </View>

      <View style={S.buttonContainer}>
	<PrimaryButton 
    	  disabled={loading}
	  onPress={send}>Enviar imagen</PrimaryButton>
      </View>
    </>
  )
}
export default FinalCount

FinalCount.navigationOptions = {
  drawerLabel: 'Cuenta Final',
  drawerIcon: (focused, tintColor) => <Icon name="counter" size={24} color={tintColor} />,
}

function Loading(props) {
  return (
    <View style={{
      	flex: 1,
	backgroundColor: 'white',
    }}>
    </View>
  )
}

const S = StyleSheet.create({
  buttonContainer: {
    padding: 10
  }
})

