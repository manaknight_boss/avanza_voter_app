import { Text } from 'react-native-paper';
import React from 'react';
import styled from 'styled-components/native'

export function Table(props) {
  const { headings, data, action:Action, pay } = props
  const length = data.length
  const isLast = i => i % length  === length -1 && !Action

  return (
    <>
      {data.map((row, r) => (
        <Row
          key={r} 
          odd={r%2 === 0}>
          {row.map((col, c) => (
            <Col 
              key={c} 
              last={isLast(c)}>
              <Text>{col}</Text>
            </Col>
          ))}

          {Action && <Col last><Action row={row} /></Col>}
        </Row>
      ))}
    </>
  )
}

export const TableHead = ({headings = []}) => {
  const length = headings.length
  return (
    <Row>
      {headings.map((h,i) => (
        <Col centered key={i} last={i % length === length - 1}>
          <H>{h}</H>
        </Col>
      ))}
    </Row>
  )
}

export const Row = styled.View`
background-color: ${({odd}) => odd ? "#F3F3F3" : 'white'};
flex-direction: row;
align-items: stretch;
`

export const Col = styled.View`
flex: 1;
align-items: ${props => props.centered ? 'center' : 'flex-start'};
justify-content: center;
padding: 5px ;
border-color: #E4E4E4;
border-right-width: ${({last}) => last ? '0' : '1px' };
border-right-color: black;
`

export const H = styled(Text)`
font-weight: bold;
padding: 10px 0;
`
