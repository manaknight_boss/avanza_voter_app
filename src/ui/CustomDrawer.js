import {
  Avatar,
  Divider,
  Drawer,
  Headline,
  Menu,
  Text
} from "react-native-paper";
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  View,
  Linking, Platform
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import React from "react";
import styled from "styled-components/native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setCurrentTable } from "../redux/actions";

function CustomDrawer(props) {
  const {
    items,
    activeItemKey,
    onItemPress,
    renderIcon,
    getLabel,
    activeTintColor,
    inactiveTintColor,
    iconContainerStyle,
    navigation,

    profile,
    currentTable
  } = props;

  // console.log("drawer", props);

  return (
    <>
      <ScrollView>
        <SafeAreaView
          style={S.container}
          forceInset={{ top: "always", horizontal: "never" }}
        >
          <Drawer.Section style={S.drawerHeader}>
            <HeaderContent>
              <Headline>
                {profile.first_name} {profile.last_name}
              </Headline>
              <View style={S.tableInfo}>
                <MenuAnchorContainer />
                <Text>{profile["recinto" + currentTable]}</Text>
              </View>
            </HeaderContent>
          </Drawer.Section>

          {items.map((route, index) => {
            const tintColor = focused ? activeTintColor : inactiveTintColor;
            const focused = route.key === activeItemKey;
            const scene = { route, index, focused, tintColor };
            const icon = renderIcon(scene);
            const label = getLabel(scene);

            return (
              <Drawer.Item
                key={index}
                label={label}
                icon={() => (
                  <IconContainer icon={icon} style={iconContainerStyle} />
                )}
                active={focused}
                onPress={() => onItemPress({ route, focused })}
                style={{ borderRadius: 30 }}
              />
            );
          })}
        </SafeAreaView>
      </ScrollView>
      <View>
        <Drawer.Item
          label="Asistencia"
          icon={() => <Icon name="phone" size={24} style={{ marginLeft: 5 }} />}
          onPress={() => {
            let phoneNumber = "8295021366";
            if (Platform.OS === 'android') { phoneNumber = `tel:${phoneNumber}`; }
            else { phoneNumber = `telprompt:${phoneNumber}`; }
            Linking.canOpenURL(phoneNumber)
              .then(supported => {
                if (!supported) {
                  Alert.alert('Phone number is not available');
                } else {
                  Linking.openURL(phoneNumber);
                }
              })
              .catch(err => console.log(err));
          }}
          style={{ borderRadius: 30, marginVertical: 15 }}
        />
      </View>

      <View>
        <Drawer.Item
          label="Salir"
          icon={() => (
            <Icon name="logout" size={24} style={{ marginLeft: 5 }} />
          )}
          onPress={() => navigation.navigate("Login")}
          style={{ borderRadius: 30, marginVertical: 15 }}
        />
      </View>
    </>
  );
}

export default connect(
  ({ profile, currentTable }) => ({ currentTable, profile }),
  dispatch => bindActionCreators({ setCurrentTable }, dispatch)
)(CustomDrawer);

const IconContainer = ({ icon, ...props }) => {
  return <View {...props}>{icon}</View>;
};

const MenuAnchor = props => {
  const { profile, currentTable, setCurrentTable } = props;
  const [visible, setVisible] = React.useState(false);

  const openMenu = () => {
    setVisible(true);
  };

  const closeMenu = () => {
    setVisible(false);
  };

  const select = table => {
    setCurrentTable(table);
    closeMenu();
  };

  if (currentTable === null) setCurrentTable(1);

  return (
    <Menu
      visible={visible}
      onDismiss={closeMenu}
      anchor={
        <TouchableOpacity onPress={openMenu}>
          <View style={S.menuAnchor}>
            <Text>{profile[`table${currentTable}`]}</Text>
            <Icon name="menu-down" size={24} />
          </View>
        </TouchableOpacity>
      }
    >
      <Menu.Item onPress={() => select(1)} title={profile.table1} />
      <Menu.Item onPress={() => select(2)} title={profile.table2} />
      <Menu.Item onPress={() => select(3)} title={profile.table3} />
    </Menu>
  );
};

const MenuAnchorContainer = connect(
  ({ currentTable, profile }) => ({ profile, currentTable }),
  dispatch => bindActionCreators({ setCurrentTable }, dispatch)
)(MenuAnchor);

const HeaderContent = styled.View`
  padding: 0 15px;
`;
const S = StyleSheet.create({
  drawerHeader: {
    paddingTop: 50
  },
  container: {
    flex: 1
  },
  avatar: {
    marginVertical: 10
  },
  menuAnchor: {
    flexDirection: "row",
    alignItems: "center"
  },

  table: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 5
  },
  tableInfo: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  }
});
