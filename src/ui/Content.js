import { KeyboardAvoidingView, ScrollView } from 'react-native';
import React from 'react';
import styled from 'styled-components/native'

function Content({children, ...props}) {
  return (
    <KeyboardAvoidingView style={{flex:1}} behavior="height">
      <ScrollView style={{flex:1}} {...props}>
        <ContentPadding>
          {children}
        </ContentPadding>
      </ScrollView>
    </KeyboardAvoidingView>
  )
}
export default Content

const ContentPadding = styled.View`
padding: 0 30px;
`

