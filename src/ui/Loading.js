import React from 'react'
import styled from 'styled-components'
import { ActivityIndicator } from 'react-native-paper';

function Loading(props){
  const {show, others} = props

  if (show === false) return null

  return (
    <Backdrop {...others}>
    	<ActivityIndicator size='large' color='orange' animating={true} />
    </Backdrop>
  );
}

export default Loading

const Backdrop = styled.View`
position: absolute;
top: 0;
left:0;
width: 100%;
height: 100%;

justify-content: center;
align-items: center;

background-color: white;
`;
