import { createSwitchNavigator } from "react-navigation";

import DrawerNavigator from "./DrawerNavigator";
import LoginScreen from "../screens/auth/LoginScreen";
import Register from "../screens/auth/Register";
import LoginStack from "./stacks/LoginStack";

const SwitchNavigator = createSwitchNavigator(
  {
    Login: LoginStack,
    App: DrawerNavigator
  },
  {
    initialRouteName: "Login"
  }
);

export default SwitchNavigator;
