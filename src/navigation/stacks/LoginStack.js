import {createStackNavigator} from 'react-navigation-stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import React from 'react';

import LoginScreen from '../../screens/auth/LoginScreen';
import Register from '../../screens/auth/Register';

const LoginStack = createStackNavigator({
  LoginScreen: LoginScreen,
  Register: Register
}, {
  initialRouteName: 'LoginScreen',
  headerMode: 'none',
  navigationOptions: {
    drawerLabel: "Resumen",
    drawerIcon: (focused, tintColor) => <Icon name="clipboard-text-outline" size={24} color={tintColor} />
  }
})
export default LoginStack