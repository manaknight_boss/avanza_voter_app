import axios from 'axios'
import store from '../redux/store'
import {loginAction, refreshAction} from '../redux/actions'

const URL = 'https://avanza360.app/v1/api'

const client =  axios.create({
  baseURL: URL,
  headers: {
    'content-type': 'application/json'
  }
})

client.interceptors.request.use((config) => {
  const {bearerToken} = store.getState()
  const {baseURL, url, method, data, headers} = config

  console.log(method.toUpperCase(), baseURL + url)
  console.log('REQUEST:', data) 

  config.headers.common.Authorization = bearerToken

  return config;
}, error => {
  return Promise.reject(error);
});

client.interceptors.response.use(response => {
  console.log('RESPONSE:', response.data, '\n')
  return response.data;
}, error => {
  const {response } = error
  let message = `El servidor ha respondido: ${response.status}`

  if (response) {
    console.log('ERROR:', error.message)
    const {data} = response

    if (data) {
      console.log(data, '\n')

      let {error} = data

      message = error 
	?  Object.entries(error).map(([key, value], i) => `[${i + 1}] ${value}`).join("\n") 
	: data.message || message

    }
  }

  return Promise.reject(new Error(message));
});


export async function login(email, password) {
  const data =  await client.post('/grassroot/login', { email, password }) 
  const token = "Bearer " + data.access_token.token
  const exp = parseInt(data.expire_in)
  const refresh_token = data.refresh_token

  store.dispatch(loginAction(token, exp, refresh_token))

  return data.success
}

export async function refresh() {
  const {refreshToken} = store.getState()
  const data = await client.post('/grassroot/token', { token:refreshToken }) 
  const {access_token, expire_in} = data

  store.dispatch(refreshAction("Bearer " + access_token, parseInt(expire_in)))

  setTimeout(() => refresh(), 60000)

  return data.success
}

export async function getProfile() {
  const data = await client.get('/grassroot/profile')
  const { data: profile } = data
  return profile
}

export async function addTurns(turns, table) {
  const data = await client.post(`/voted/municipal/${table}`, { voter_turns: turns })
  return true

}


export async function searchRecinto(govId) {
  const data = await client.get(`/government/${govId}`)
  const {data:recinto} = data;
  return recinto
}

export async function getTableResults(table) {
  const data = await client.get(`/table/results/municipal/${table}`)
  const {data: results} = data
  return results
}

export async function getCoordinator(coordinatorId, table) {
  const data = await client.get(`/table/cordinator/municipal/${coordinatorId}/${table}`)
  const {data: result} = data
  return result;
}

export async function uploadFinalCount(table, uri) {
  const formData = new FormData()
  formData.append('table', table )
  formData.append('file', {
    uri,
    type: 'image/jpeg',
    name: 'FinalCountImage'
  })
  const data = await client.post('/final/count', formData)
  return data.success
}

export function getCurrentTable() {
  const {profile} = store.getState()
  const {currentTable} = store.getState()
  return profile["table"+currentTable]
}



export function getCurrentTableNumber() {
  const {currentTable} = store.getState()
  return currentTable;
}
