Instructions
-----------------------------

## Setup

- Install expo: `npm i -g expo-cli`
- Install dependencies:
```bash
$ npm install
# or
$ yarn
```

## Running


1. Start Expo

```
$ expo start
```

2. Scan the QR code
